// Fill out your copyright notice in the Description page of Project Settings.


#include "Open_Close_Component.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "UObject/NameTypes.h"
#include "GrabberComponent.h"
#include "Components/StaticMeshComponent.h"

#define GETPLAYER GetWorld()->GetFirstPlayerController()->GetPawn()

// Sets default values for this component's properties
UOpen_Close_Component::UOpen_Close_Component()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpen_Close_Component::BeginPlay()
{
	Super::BeginPlay();

	TriggerComponent = GetOwner()->FindComponentByClass<UBoxComponent>();
	
	this->Player = GetWorld()->GetFirstPlayerController()->GetPawn();
	this->InitialDoorRotation = GetOwner()->GetActorRotation();
	this->m_InitialLocation = GetOwner()->GetActorLocation();
	this->m_FinalLocation = GetOwner()->GetActorLocation() + this->OpeningValue;

	FVector m_finalRotation = GetOwner()->GetActorRotation().Vector() + this->OpeningValue;
	this->m_FinalRotation = FQuat(FRotator(m_finalRotation.X, m_finalRotation.Y, m_finalRotation.Z));


	Open = false;
	PlayerInRange = false;
	this->PlayerCanInteract = true;
	this->TimeToOpen /= 3;

	//Get total required mass to activate door if dependent on pressure plates
	if (m_LockType == ELocks::PRESSUREPLATE)
	{
		if (m_dependentPressurePlates.Num() > 0)
		{
			float totalMassNeeded = 0;
			for (APressurePlate_base* pressurePlate : m_dependentPressurePlates)
			{
				totalMassNeeded += pressurePlate->m_minRequiredWeight;
			}
			m_totalMassNeeded = totalMassNeeded;
		}
		else
		{
			FString errorMessage = GetOwner()->GetName() + ": " + "is not referencing and pressure plate it depends on";
			UE_LOG(LogTemp, Error, TEXT("%s"), *errorMessage)
		}
	}
}


// Called every frame
void UOpen_Close_Component::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	PercentageRate = (1 / this->TimeToOpen) * DeltaTime;

	//Pointer protection
	if (!TriggerComponent) { return; }

	m_Location = GetOwner()->GetActorLocation();

	if (m_LockType == ELocks::PRESSUREPLATE)
	{
		CheckPressurePlatesStatus();
	}

	if (Open)
	{
		TryOpeningActor();
		IsOpening = true;
		IsClosing = false;
	}
	else
	{
		TryClosingActor();
		IsOpening = false;
		IsClosing = true;
	}
}

void UOpen_Close_Component::CheckRotationState()
{
	this->m_Rotation = GetOwner()->GetActorRotation();
	
	//GetOwner()->SetActorRotation(FMath::Lerp(FQuat(this->m_Rotation), FQuat(this->m_FinalRotation), CurrentTime));
	//GetOwner()->SetActorRelativeRotation(FMath::Lerp(FQuat(this->m_Rotation), FQuat(this->m_FinalRotation), CurrentTime));
	GetOwner()->SetActorRelativeRotation(FMath::Lerp(FQuat(this->m_Rotation), FQuat(this->FinalRotation), PercentageRate));

}

void UOpen_Close_Component::CheckTranslationState()
{
	
	m_Location = GetOwner()->GetActorLocation();

	GetOwner()->SetActorLocation(FMath::Lerp(m_Location, m_FinalLocation, PercentageRate));

}

void UOpen_Close_Component::CloseRotation()
{
	this->m_Rotation = GetOwner()->GetActorRotation();
	//GetOwner()->SetActorRotation(FMath::Lerp(FQuat(this->m_Rotation), FQuat(this->InitialDoorRotation), CurrentTime));
	GetOwner()->SetActorRelativeRotation(FMath::Lerp(FQuat(this->m_Rotation), FQuat(this->InitialDoorRotation), PercentageRate));
}

void UOpen_Close_Component::CloseTranslation()
{
	m_Location = GetOwner()->GetActorLocation();
	GetOwner()->SetActorLocation(FMath::Lerp(m_Location, m_InitialLocation, PercentageRate));
}

void UOpen_Close_Component::TryOpeningActor()
{
	switch (m_InteractionType)
	{
	case EInteractionTypes::ROTATES:
		CheckRotationState();
		break;

	case EInteractionTypes::TRANSLATES:
		CheckTranslationState();
		break;

	default:
		FString ActorName = GetOwner()->GetName();
		UE_LOG(LogTemp, Error, TEXT("%s is missing an interaction type"), *ActorName);
		break;
	}
}

void UOpen_Close_Component::TryClosingActor()
{
	switch (m_InteractionType)
	{
	case EInteractionTypes::ROTATES:
		CloseRotation();
		break;

	case EInteractionTypes::TRANSLATES:
		CloseTranslation();
		break;

	default:
		FString ActorName = GetOwner()->GetName();
		UE_LOG(LogTemp, Error, TEXT("%s is missing an interaction type"), *ActorName);
		break;
	}
}

void UOpen_Close_Component::CheckPressurePlatesStatus()
{
	Open = AllDependentPressurePlatesAreActive() ? true : false;
}

float UOpen_Close_Component::GetTotalCurrentMass()
{
	if (m_dependentPressurePlates.Num() < 1) return 0;
	float totalCurrentMass = 0;
	for (APressurePlate_base* pressurePlate : m_dependentPressurePlates)
	{
		totalCurrentMass += pressurePlate->GetTotalCurrentMass();
	}
	return totalCurrentMass;
}

bool UOpen_Close_Component::AllDependentPressurePlatesAreActive()
{
	if (m_dependentPressurePlates.Num() == 0) return false;
	bool allActive = true;
	for (APressurePlate_base* dependentPlat : m_dependentPressurePlates)
	{
		if (!dependentPlat->IsActivated())
		{
			allActive = false;
			break;
		}
	}
	return allActive;
}

bool UOpen_Close_Component::ActorHasFullyOpenedOrClosed()
{
	if (m_InteractionType == EInteractionTypes::ROTATES)
	{

		///Use sum of all vector components while rotating, to find the difference between the target and the current rotation
		float currRotSum = this->m_Rotation.Vector().X;
		currRotSum += this->m_Rotation.Vector().Y;

		float finalRotSum = this->FinalRotation.Vector().X;
		finalRotSum += this->FinalRotation.Vector().Y;

		float rotationDiff = finalRotSum - currRotSum;

		if (FMath::Abs(rotationDiff) < 0.1f)
		{
			//Player can try to open or close door again
			return true;
		}

	}
	else if (m_InteractionType == EInteractionTypes::TRANSLATES)
	{
		float diffToEndLocation = FVector::Dist(m_FinalLocation, m_Location);
		float diffToStartLocation = FVector::Dist(m_InitialLocation, m_Location);
		if (diffToEndLocation <= 1.f || diffToStartLocation <= 1.f)
		{
			return true;
			CurrentPercentage = 0;
		}
	}

	return false;
}

bool UOpen_Close_Component::IsOpen()
{
	return Open;
}

bool UOpen_Close_Component::Rotates()
{
	if (m_InteractionType == EInteractionTypes::ROTATES)return true;
	return false;
}

void UOpen_Close_Component::SetPlayerInRange(bool tempRange)
{
	this->PlayerInRange = tempRange;
}

void UOpen_Close_Component::FlipFlopOpen()
{
	m_InteractedFired = true;
	Open = !Open;
}


