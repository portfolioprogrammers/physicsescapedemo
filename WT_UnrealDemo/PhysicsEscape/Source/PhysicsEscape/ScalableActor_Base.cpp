// Fill out your copyright notice in the Description page of Project Settings.


#include "ScalableActor_Base.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "GrabberComponent.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

#define OUT
#define GETPLAYER GetWorld()->GetFirstPlayerController()->GetPawn()
// Sets default values
AScalableActor_Base::AScalableActor_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Scalable Mesh"));
	RootComponent = m_mesh;
}

// Called when the game starts or when spawned
void AScalableActor_Base::BeginPlay()
{
	Super::BeginPlay();
	m_collider = FindComponentByClass<UBoxComponent>();
	m_collider->OnComponentEndOverlap.AddDynamic(this, &AScalableActor_Base::OnOverlapEnd);
	m_spawnPosition = GetActorLocation();
}

// Called every frame
void AScalableActor_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	m_renderOutline = false;
	CheckActorReSpawn();
}

void AScalableActor_Base::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (!m_collider || !m_volumeOutline)return;
	FString OtherActorName = OtherActor->GetName();
	FString volumeName = m_volumeOutline->GetName();
	UE_LOG(LogTemp,Warning, TEXT("Other actor %s referenced volume outline %s"), *OtherActorName, *volumeName) 
	if (OtherActor != nullptr && OtherActor->GetName() == m_volumeOutline->GetName())
	{
		UGrabberComponent* playerGrabber = GETPLAYER->FindComponentByClass<UGrabberComponent>();
		if (playerGrabber)
		{
			if (playerGrabber->scalableActor != nullptr && playerGrabber->scalableActor->GetName() == this->GetName())
			{
				playerGrabber->RespawnItemGrabbed();
				return;
			}
			
		}

		m_HasLeftVolumeRestrictions = true;
	}
}

void AScalableActor_Base::CheckActorReSpawn()
{
	if (!m_collider || !m_volumeOutline)return;

	if (m_HasLeftVolumeRestrictions)
	{
		SetActorLocation(m_spawnPosition);
		m_HasLeftVolumeRestrictions = false;
	}
}

float AScalableActor_Base::GetTotalMass()
{
	float totalMass = m_mesh->GetMass();

	TArray<AActor*> overlappingActors;
	m_mesh->GetOverlappingActors(OUT overlappingActors);
	for(AActor* actor : overlappingActors)
	{
		AScalableActor_Base* interactableActor = Cast<AScalableActor_Base>(actor);
		if (interactableActor != nullptr)
		{
			totalMass += interactableActor->GetTotalMass();
		}
	}
	return totalMass;
}

