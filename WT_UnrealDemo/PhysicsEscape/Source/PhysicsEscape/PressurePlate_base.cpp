// Fill out your copyright notice in the Description page of Project Settings.


#include "PressurePlate_base.h"
#include "ScalableActor_Base.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

#define OUT

// Sets default values
APressurePlate_base::APressurePlate_base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = m_mesh;

}

// Called when the game starts or when spawned
void APressurePlate_base::BeginPlay()
{
	Super::BeginPlay();
	m_trigger = FindComponentByClass<UBoxComponent>();
}

// Called every frame
void APressurePlate_base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	m_CurrentMass = 0;

	TArray<AActor*> overlappingActors;
	m_trigger->GetOverlappingActors(OUT overlappingActors);
	for(AActor* actor : overlappingActors)
	{
		AScalableActor_Base* interactableActor = Cast<AScalableActor_Base>(actor);
		if (interactableActor != nullptr)
		{
			m_CurrentMass += interactableActor->GetTotalMass();
		}
		else
		{
			float actorMass = actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
			m_CurrentMass += actorMass;
		}
	}

	if (m_CurrentMass > m_minRequiredWeight)
	{
		FString STotalMass = FString::SanitizeFloat(m_CurrentMass);
		UE_LOG(LogTemp, Warning, TEXT("%s"), *STotalMass)
		UE_LOG(LogTemp, Warning, TEXT("Pressure plate activated"))
		m_Activated = true;
		
	}
	else
	{
		m_Activated = false;
	}
}


