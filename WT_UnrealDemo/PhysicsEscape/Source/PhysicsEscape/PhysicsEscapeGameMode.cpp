// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PhysicsEscapeGameMode.h"
#include "PhysicsEscapeHUD.h"
#include "PhysicsEscapeCharacter.h"
#include "UObject/ConstructorHelpers.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ScalableActor_Base.h"

#include "Engine.h"


APhysicsEscapeGameMode::APhysicsEscapeGameMode()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APhysicsEscapeHUD::StaticClass();
}

void APhysicsEscapeGameMode::Tick(float DeltaTime) 
{
	Super::Tick(DeltaTime);
	//FRotator Rotator = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraRotation();

	//TArray<AActor*> FoundActors;
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), AScalableActor_Base::StaticClass(), FoundActors);

	//if (FoundActors.Num() > 0)
	//{
	//	//UE_LOG(LogTemp, Warning, TEXT("%d"), FoundActors.Num())
	//	AActor* Cube = FoundActors[0];
	//	Cube->SetActorRotation(Rotator.Quaternion().Inverse());

	//	FVector PlayerViewPointLocation;
	//	FVector EndPoint;
	//	FRotator PlayerViewPointRotation;
	//	//TODO create an offset from the player viewpoint, so that the gyroscope is not in the middle of the screen
	//	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
	//		OUT	PlayerViewPointLocation,
	//		OUT	PlayerViewPointRotation
	//	);

	//	EndPoint = PlayerViewPointLocation + (PlayerViewPointRotation.Vector() * 400.f);

	//	Cube->SetActorLocation(EndPoint);

	//}
}

void APhysicsEscapeGameMode::BeginPlay()
{
	Super::BeginPlay();

	///*APlayerController* PlayerController = GEngine->GetFirstLocalPlayerController(GetWorld());
	//PlayerController->bShowMouseCursor = true;
	//Gyroscope = ACGyroscope::Spawn(GetWorld(), FVector::ZeroVector, FRotator::ZeroRotator);
	//Gyroscope->SetActorLabel("!GYRO");

	//Gyroscope->AttachToActor(PlayerController->PlayerCameraManager, FAttachmentTransformRules::KeepWorldTransform);
	//Gyroscope->SetActorRelativeLocation(FVector(400, -200, -100));*/
}
