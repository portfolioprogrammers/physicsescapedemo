// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PhysicsEscapeGameMode.generated.h"


UCLASS(minimalapi)
class APhysicsEscapeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APhysicsEscapeGameMode();
	virtual void Tick(float DeltaTime) override;
protected:
	virtual void BeginPlay()override;
	//Woah nelly
};



