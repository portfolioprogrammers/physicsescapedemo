// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GrabberComponent.h"
#include "FP_Controller.generated.h"

USTRUCT(BlueprintType)
struct FInteractionManager
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		bool Interacting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		float Scalar = false;
};
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PHYSICSESCAPE_API UFP_Controller : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFP_Controller();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	//Get Components
	UGrabberComponent* m_grabber = nullptr;
	APlayerController* m_PC = nullptr;

	AActor* m_pickuableItem = nullptr;

	//Tracking properties
	
	bool m_inspecting = false;

public:
	//TODO Find a better way to get information from the player controller, without mixing up Blueprints and C++--> too hard-coded
	UPROPERTY(BlueprintReadWrite)
		FInteractionManager m_IM = FInteractionManager();
	UPROPERTY(BlueprintReadWrite)
		bool m_Interacting;
	UPROPERTY(BlueprintReadWrite)
		float m_Scalar;
	UPROPERTY(BlueprintReadWrite)
		float MOUSEX;
	UPROPERTY(BlueprintReadWrite)
		float MOUSEY;
	UPROPERTY(BlueprintReadWrite)
		bool m_Inspecting;
	UPROPERTY(EditAnywhere)
		float dynamicInteractionBufferTime;
	UPROPERTY(BlueprintReadWrite)
		bool m_currentlyHoldingItem = false;

private:
	void UpdateInteractableActors();
	void UpdateDynamicInteractions();

	bool m_TryOpeningOrClosing = false;
	//Time Buffer for dynamic interactions
	
	float dynamicHandlerTimer = 0;

};
