// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/BoxComponent.h"
#include "GrabberComponent.generated.h"

///TODO Team decision if input manager should be in a separate Pawn/Player Controller script OR in the player main Blueprint 

///Track any use any grabbing interactions in Blueprint and C++ dynamically
class AScalableActor_Base;

USTRUCT(BlueprintType)
struct FLineTraceResults
{
	GENERATED_USTRUCT_BODY()
		FLineTraceResults(AActor* tempActorHit = nullptr, UPrimitiveComponent* tempComponent = nullptr) 
	{
		actorHit = tempActorHit;
		component = tempComponent;
	}
public: 
	AActor* actorHit = nullptr;
	UPrimitiveComponent* component = nullptr;
};
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PHYSICSESCAPE_API UGrabberComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabberComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
public:
	UFUNCTION()
		void GrabItem(AActor* objHit, UPrimitiveComponent* component);
	UFUNCTION()
		void Release();
	UFUNCTION()
		void RespawnItemGrabbed();
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
public:
	//Line trace methods
	FLineTraceResults GetFirstPhysicsBodyInReach();
	FLineTraceResults GetFirstDynamicBodyInReach();//TODO in case the first body hit returned isnt enough, we will return a pointer to an array of hitresults and use line trace multi by objet type
	void ScaleInteractedActor(float);
	void SetTargetLocation();
private:
	//Line trace properties
	FVector m_lineTraceEnd;
	UPhysicsHandleComponent* m_physicsHandlePtr = nullptr;
	
	UPROPERTY(EditAnywhere)
		float m_castDistance = 50; //Its in cm
	UPROPERTY(EditAnywhere)
		FVector m_relativeDistance = FVector::ZeroVector;
public:
	UPROPERTY(BlueprintReadWrite)
		AActor* interactedActor = nullptr;
	UPROPERTY(BlueprintReadWrite)
		AScalableActor_Base* scalableActor = nullptr;
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
public:
	//Rotating physics interacted Actor
	void RotateInteractedActor(float mouseX, float mouseY);
	void RenderInteractedActorOutline(bool render);
private:
	bool HandlingObject = false;
	UPROPERTY(EditAnywhere)
		float m_turnRate;
public:
	bool IsHandlingAnObject();	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
private:
	bool m_HasKey = false;
public:
	void AcquireKey() { m_HasKey = true; }
	void UseKey() { m_HasKey = false; }
	inline bool CurrentlyHoldingKey() { return m_HasKey; }
};
