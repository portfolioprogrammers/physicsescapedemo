// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TriggerBox.h"
#include "Components/BoxComponent.h"
#include "ScalableActor_Base.generated.h"

UCLASS()
class PHYSICSESCAPE_API AScalableActor_Base : public AActor
{
	GENERATED_BODY()
public:

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Scalable Mesh")
		UStaticMeshComponent* m_mesh;

public:	
	// Sets default values for this actor's properties
	AScalableActor_Base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

//Base properties
public:
	UPROPERTY(EditAnywhere)
		FVector m_minScale;
	UPROPERTY(EditAnywhere)
		FVector m_maxScale;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool IsScalable;
	UPROPERTY(VisibleAnywhere)
		float m_scaleRate;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Scaling Properties")
		bool m_renderOutline;
	
private:
	UPROPERTY(EditAnywhere, Category = "Volume Limitation")
		ATriggerBox* m_volumeOutline = nullptr;
	
private:
	UBoxComponent* m_collider = nullptr;
	FVector m_spawnPosition;
	bool m_HasLeftVolumeRestrictions = false;
	virtual void CheckActorReSpawn();
public: 
	virtual float GetTotalMass();
	inline FVector GetSpawnLocation() { return m_spawnPosition; }
};
