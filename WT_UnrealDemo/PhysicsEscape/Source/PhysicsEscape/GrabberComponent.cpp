// Fill out your copyright notice in the Description page of Project Settings.


#include "GrabberComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Misc/App.h"
#include "ScalableActor_Base.h"
#include "Camera/CameraComponent.h"
//This class belongs to NEBULA GAMES
#include "C6DoF_s.h"

#define OUT 

// Sets default values for this component's properties
UGrabberComponent::UGrabberComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabberComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	m_physicsHandlePtr = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (m_physicsHandlePtr == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Player is missing PhysicsHandleComponent"))
	}

}


void UGrabberComponent::GrabItem(AActor* objHit, UPrimitiveComponent* component)
{
	//Results for physics body trace
	/*auto ComponentGrabbed = physicsHitResult.GetComponent();
	auto p_ActorHit = physicsHitResult.GetActor();*/
	if (!scalableActor)return;
	//If a physics body was hit and we try to interact with it, then grab
	if (objHit)
	{

		FVector PlayerViewPointLocation;
		FRotator PlayerViewPointRotation;
		GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
			OUT	PlayerViewPointLocation,
			OUT	PlayerViewPointRotation
		);
		
		FVector playerPos = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		objHit->SetActorLocation(PlayerViewPointLocation + (PlayerViewPointRotation.Vector() * (m_castDistance / 3)),true);
		FVector locationOffset = PlayerViewPointLocation + (PlayerViewPointRotation.Vector() * (m_castDistance / 3));
		m_physicsHandlePtr->GrabComponentAtLocationWithRotation(
			component,
			NAME_None,
			component->GetOwner()->GetActorLocation(),
			component->GetOwner()->GetActorRotation()
		);
		component->SetSimulatePhysics(false);
		//scalableActor->m_mesh->SetPhysicsLinearVelocity(FVector::ZeroVector);
	}
}

void UGrabberComponent::Release()
{
	if (!scalableActor)return;
	//scalableActor->m_mesh->SetSimulatePhysics(true);
	m_physicsHandlePtr->ReleaseComponent();
	UPrimitiveComponent* ScalableMesh =  scalableActor->FindComponentByClass<UPrimitiveComponent>();
	ScalableMesh->SetSimulatePhysics(true);
	
	/*if (scalableActor->GetVelocity().Size() >= 500)
	{
		scalableActor->m_mesh->SetPhysicsLinearVelocity(FVector::UpVector * 150);
	}
	*/
	
}

void UGrabberComponent::RespawnItemGrabbed()
{
	if (!scalableActor) return;

	//Reset scalable actor location
	FVector initialPosition = scalableActor->GetSpawnLocation();
	scalableActor->SetActorLocation(initialPosition);
	//Reset pointer
	scalableActor = nullptr;
}

FLineTraceResults UGrabberComponent::GetFirstPhysicsBodyInReach()
{
	FHitResult hit;
	bool HitPhysicsInteractable;

	if (GetWorld() != nullptr)
	{
		FVector PlayerViewPointLocation;
		FRotator PlayerViewPointRotation;
		GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
			OUT	PlayerViewPointLocation,
			OUT	PlayerViewPointRotation
		);

		m_lineTraceEnd = PlayerViewPointLocation + (PlayerViewPointRotation.Vector() * m_castDistance);

		//Specify a tag if any and how precise the hit must be (false)--> block around an object and (true)-->details of the object
		FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

		//Cast a ray to reach a certain distance
		 HitPhysicsInteractable = GetWorld()->LineTraceSingleByObjectType(
			OUT	hit,
			PlayerViewPointLocation,
			m_lineTraceEnd,
			//Instead of checking for layer like in unity, you check for a specific collision channel
			FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel2)
			//TraceParams
		);
		if (HitPhysicsInteractable)
		{
			scalableActor = Cast<AScalableActor_Base>(hit.GetActor());
		}
		else if (scalableActor != nullptr)
		{
			
			Release();

			scalableActor = nullptr;

		} else 
		{
			scalableActor = nullptr;
		}
	}

	return FLineTraceResults(hit.GetActor(), hit.GetComponent());
}

FLineTraceResults UGrabberComponent::GetFirstDynamicBodyInReach()
{
	FHitResult hit;
	if (GetWorld() != nullptr)
	{
		FVector PlayerViewPointLocation;
		FRotator PlayerViewPointRotation;
		GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
			OUT	PlayerViewPointLocation,
			OUT	PlayerViewPointRotation
		);

		m_lineTraceEnd = PlayerViewPointLocation + (PlayerViewPointRotation.Vector() * m_castDistance);

		
		//Specify a tag if any and how precise the hit must be (false)--> block around an object and (true)-->details of the object
		FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

		//Cast a ray to reach a certain distance
		bool HitPhysicsInteractable = GetWorld()->LineTraceSingleByObjectType(
			OUT	hit,
			PlayerViewPointLocation,
			m_lineTraceEnd,
			//Instead of checking for layer like in unity, you check for a specific collision channel
			FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel3),
			TraceParams
		);

		
		
	}

	return FLineTraceResults(hit.GetActor(), hit.GetComponent());

}

void UGrabberComponent::ScaleInteractedActor(float scalar)
{
	
	if (!scalableActor)return;
		
	FVector maxSize = scalableActor->m_maxScale;
	FVector minSize = scalableActor->m_minScale;

	if ((maxSize.X == 0 || maxSize.Y == 0 || maxSize.Z == 0) || (minSize.X == 0 || minSize.Y == 0 || minSize.Z == 0))
	{
		FString scalableActorName = scalableActor->GetName();
		UE_LOG(LogTemp, Error,
			TEXT("Scalable actors cannot have any 0 maximum or minimum scale values. The following actor %s has registered a nonscalable value, please ensure the actor has no 0 scalable values"), *scalableActorName)
		return;
	}
	//FVector appliedScale = FVector(0.025f, 0.025f, 0.025f) * scalar;
	FVector previousScale = scalableActor->GetActorScale3D();
	
	float appliedScale = scalar * 0.025f;

	float XlimitCheck = previousScale.X;
	float YlimitCheck = previousScale.Y;
	float ZlimitCheck = previousScale.Z;

	XlimitCheck += appliedScale;
	YlimitCheck += appliedScale;
	ZlimitCheck += appliedScale;
	/*if (previousScale.X <= minSize.X && scalar < 0 || previousScale.X >= maxSize.X && scalar > 0)return;
	if (previousScale.Y <= minSize.Y && scalar < 0 || previousScale.Y >= maxSize.Y && scalar > 0)return;
	if (previousScale.Z <= minSize.Z && scalar < 0 || previousScale.Z >= maxSize.Z && scalar > 0)return;
		*/
	 XlimitCheck = FMath::Clamp(XlimitCheck, FMath::Abs(minSize.X), FMath::Abs(maxSize.X));
	 YlimitCheck = FMath::Clamp(YlimitCheck, FMath::Abs(minSize.Y), FMath::Abs(maxSize.Y));
	 ZlimitCheck = FMath::Clamp(ZlimitCheck, FMath::Abs(minSize.Z), FMath::Abs(maxSize.Z));

	 FVector resultingScale = FVector(XlimitCheck, YlimitCheck, ZlimitCheck);

	scalableActor->SetActorScale3D(resultingScale);	
	
}

void UGrabberComponent::SetTargetLocation()
{
	m_physicsHandlePtr->SetTargetLocation(GetOwner()->GetActorLocation() + m_relativeDistance);
}

void UGrabberComponent::RotateInteractedActor(float mouseX, float mouseY)
{
	if (!scalableActor)return;


	FVector SafeAxisUp = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetRootComponent()->GetRightVector();
	FVector SafeAxisRight = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetRootComponent()->GetUpVector();
	
	FRotator DeltaRotationRight = FQuat(SafeAxisRight, FMath::DegreesToRadians(mouseX * m_turnRate)).Rotator();
	scalableActor->AddActorWorldRotation(DeltaRotationRight);

	FRotator DeltaRotationUp = FQuat(SafeAxisUp, FMath::DegreesToRadians(mouseY * m_turnRate)).Rotator();
	scalableActor->AddActorWorldRotation(DeltaRotationUp);

	
	 
	//UCameraComponent* playerCam = GetOwner()->FindComponentByClass<UCameraComponent>();
	//if (!playerCam) return;
	//FTransform camTransform = playerCam->GetComponentTransform();
	//FVector playerRight = GetOwner()->GetActorRightVector();
	//FVector camLocalRight = camTransform.TransformVectorNoScale(playerCam->GetUpVector());
	//FVector camLocalUp = camTransform.TransformVectorNoScale(playerCam->GetRightVector());

	////scalableActor->GetActorTransform().InverseTransformVectorNoScale()
	////Convert all Eulers into Quarternions
	//FVector pitchV = (mouseY * m_turnRate) * camLocalRight.GetSafeNormal();
	//FRotator pitch_T (pitchV.X, pitchV.Y, pitchV.Z);
	//FQuat QPitch_T = UC6DoF_s::Euler_To_Quaternion(pitch_T);

	//FVector yawV = (mouseX * m_turnRate) * camLocalUp.GetSafeNormal();
	//FRotator yaw_T(yawV.X, yawV.Y, yawV.Z);
	//FQuat QYaw_T = UC6DoF_s::Euler_To_Quaternion(yaw_T);

	//FRotator roll_T(0, mouseY * m_turnRate,0);
	//FQuat QRoll_T = UC6DoF_s::Euler_To_Quaternion(roll_T);

	////Add local rotations to inspected actor
	//UC6DoF_s::AddActorLocalRotationQuat(scalableActor, QPitch_T);
	//UC6DoF_s::AddActorLocalRotationQuat(scalableActor, QYaw_T);
	//UC6DoF_s::AddActorLocalRotationQuat(scalableActor, QRoll_T);

	////Get referenced actor rotation and store it
	//FRotator previousRotation = scalableActor->GetActorRotation();
	////Combine previous rotation with mouse rotation * custom rate speed 
	//FRotator addedRotation = FRotator(mouseY, mouseY, mouseX) * m_turnRate;
	////set actor rotation to be new combined rotation
	//scalableActor->SetActorRotation((previousRotation + addedRotation).Quaternion());

	/*FRotator RotationDelta(mouseY , mouseY , mouseX );
	FTransform NTransform = scalableActor->GetActorTransform();
	NTransform.ConcatenateRotation(RotationDelta.Quaternion());
	NTransform.NormalizeRotation();
	scalableActor->SetActorTransform(NTransform);*/
}

void UGrabberComponent::RenderInteractedActorOutline(bool render = true)
{
	//FString yeehaw = render?TEXT("true"):TEXT("false");
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *yeehaw);
	if (!scalableActor)return;
	
	scalableActor->m_renderOutline = render;
	
}

bool UGrabberComponent::IsHandlingAnObject()
{
	return HandlingObject;
}

