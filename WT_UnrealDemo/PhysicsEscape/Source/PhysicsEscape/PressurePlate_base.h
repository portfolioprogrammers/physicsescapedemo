// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PressurePlate_base.generated.h"

class UBoxComponent;
UCLASS()
class PHYSICSESCAPE_API APressurePlate_base : public AActor
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Pressure Mesh")
		UStaticMeshComponent* m_mesh;

public:	
	// Sets default values for this actor's properties
	APressurePlate_base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	UPROPERTY(EditAnywhere)
		float m_minRequiredWeight;
	UPROPERTY(BlueprintReadOnly)
		bool m_Activated = false;
private:
	UBoxComponent* m_trigger;


protected:
	float m_CurrentMass;

public:
	inline float GetTotalCurrentMass() { return m_CurrentMass; }
	inline bool IsActivated() { return m_Activated; }
};
