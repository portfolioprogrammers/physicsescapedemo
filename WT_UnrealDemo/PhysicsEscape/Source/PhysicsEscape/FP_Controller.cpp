// Fill out your copyright notice in the Description page of Project Settings.


#include "FP_Controller.h"

#include "ScalableActor_Base.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

//Other Component headers
#include "KeyComp.h"
#include "Open_Close_Component.h"


// Sets default values for this component's properties
UFP_Controller::UFP_Controller()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFP_Controller::BeginPlay()
{
	Super::BeginPlay();

	// ...
	m_grabber = GetOwner()->FindComponentByClass<UGrabberComponent>();

	m_PC = GetWorld()->GetFirstPlayerController();
	
}


// Called every frame
void UFP_Controller::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	
	UpdateInteractableActors();//Player manipulation with physics handler, outline shader, scaling, and rotating
	UpdateDynamicInteractions();//All interactions with Dynamic actors

}

void UFP_Controller::UpdateInteractableActors()
{
	if (m_grabber && m_PC)
	{
		//Display shader outline on interactable object
		if (m_grabber->GetFirstPhysicsBodyInReach().actorHit)
		{
			m_grabber->RenderInteractedActorOutline(true);
		}
		else
		{
			m_grabber->RenderInteractedActorOutline(false);
		}

		
		//Check if player wants to pickup item
		if (m_Interacting && m_grabber->GetFirstPhysicsBodyInReach().actorHit)
		{
			m_grabber->RenderInteractedActorOutline(false);

			AActor* aHit = m_grabber->GetFirstPhysicsBodyInReach().actorHit;
			UPrimitiveComponent* compHit = m_grabber->GetFirstPhysicsBodyInReach().component;

			if (aHit != nullptr && compHit != nullptr && compHit->GetMass() <= 450)
			{
					m_grabber->SetTargetLocation();
				m_grabber->GrabItem(aHit, compHit);

			}

			m_currentlyHoldingItem = true;
			//compHit->SetSimulatePhysics(true);

		}
		else if (!m_Interacting && m_currentlyHoldingItem)
		{

			UPrimitiveComponent* compHit = m_grabber->GetFirstPhysicsBodyInReach().component;
			if (compHit != nullptr)
			{
					m_grabber->Release();

			}
			m_currentlyHoldingItem = false;
		}

		//Check if player wants to scale item
		if (m_Scalar != 0 && m_grabber->scalableActor != nullptr)
		{
			if (m_grabber->scalableActor->IsScalable)
			{
				UE_LOG(LogTemp, Warning, TEXT("Scaling Object"))
					//float scalar = m_IM.Scalar;
					m_grabber->ScaleInteractedActor(m_Scalar);
			}

		}

		if (m_currentlyHoldingItem && m_Inspecting)
		{
			if (MOUSEX != 0 || MOUSEY != 0)
			{
				m_grabber->RotateInteractedActor(MOUSEX, MOUSEY);
			}
		}
	}
}

void UFP_Controller::UpdateDynamicInteractions()
{
	UWorld* world = GetWorld();
	if (!m_Interacting && world->GetTimeSeconds() > dynamicHandlerTimer)
	{
		m_TryOpeningOrClosing = true;
		dynamicHandlerTimer = 0;
	}
	

	if (m_grabber->GetFirstDynamicBodyInReach().actorHit)
	{

		m_pickuableItem = m_grabber->GetFirstDynamicBodyInReach().actorHit;

		//Check what kind of collectable
		if (m_pickuableItem->FindComponentByClass<UKeyComp>() && !m_grabber->CurrentlyHoldingKey())
		{
			UKeyComp* KeyComp = m_pickuableItem->FindComponentByClass<UKeyComp>();
			KeyComp->m_renderOutline = true;
		}
	}
	else
	{
		if (m_pickuableItem != nullptr)
		{
			if (m_pickuableItem->FindComponentByClass<UKeyComp>() && !m_grabber->CurrentlyHoldingKey())
			{
				UKeyComp* KeyComp = m_pickuableItem->FindComponentByClass<UKeyComp>();
				KeyComp->m_renderOutline = false;
			}
		}
	}

	if ((m_TryOpeningOrClosing && m_Interacting) && m_grabber->GetFirstDynamicBodyInReach().actorHit)
	{
		AActor* dynamicActor = m_grabber->GetFirstDynamicBodyInReach().actorHit;

		//Grab Key
		if (dynamicActor->FindComponentByClass<UKeyComp>() && !m_grabber->CurrentlyHoldingKey())
		{
			m_grabber->AcquireKey();
			dynamicActor->Destroy();
			if (m_pickuableItem != nullptr &&  m_pickuableItem->FindComponentByClass<UKeyComp>())
			{
				m_pickuableItem = nullptr;
			}
		}

		//Open/Close Door
		if (dynamicActor->FindComponentByClass<UOpen_Close_Component>())
		{
			UOpen_Close_Component* doorProp = dynamicActor->FindComponentByClass<UOpen_Close_Component>();
			if (doorProp->ActorHasFullyOpenedOrClosed())
			{
				
				if (m_grabber->CurrentlyHoldingKey() && doorProp->GetDynamicType() == ELocks::REQUIRESKEY)
				{
					UE_LOG(LogTemp, Warning, TEXT("Try opening or closing door requiring key"))
					m_grabber->UseKey();
					doorProp->FlipFlopOpen();
				}
				else if (doorProp->GetDynamicType() == ELocks::UNLOCKED)
				{
					UE_LOG(LogTemp, Warning, TEXT("Try opening or closing door"))
					doorProp->FlipFlopOpen();
				}
			}
		}
;
		m_TryOpeningOrClosing = false;
		dynamicHandlerTimer = world->GetTimeSeconds() + dynamicInteractionBufferTime;
	}
}

