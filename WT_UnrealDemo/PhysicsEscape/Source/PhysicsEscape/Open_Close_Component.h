// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/BoxComponent.h"

#include "PressurePlate_base.h"

#include "Open_Close_Component.generated.h"

UENUM()
enum class EInteractionTypes : uint8
{
	ROTATES    UMETA(DisplayName = "Rotating"),
	TRANSLATES UMETA(DisplayName = "Translating")
};

UENUM()
enum class ELocks : uint8
{
	REQUIRESKEY      UMETA(DisplayName = "Requires Key"),
	REQUIRESPASSWORD UMETA(DisplayName = "Requires Password"),
	UNLOCKED         UMETA(DisplayName = "Unlocked"),
	PRESSUREPLATE    UMETA(DisplayName = "Pressure-plate dependent")
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PHYSICSESCAPE_API UOpen_Close_Component : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpen_Close_Component();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite)
		bool m_InteractedFired = false;

private:
	UBoxComponent* TriggerComponent = nullptr;
	UStaticMeshComponent* Door = nullptr;
	AActor* Player = nullptr;

	UPROPERTY(EditAnywhere)
		EInteractionTypes m_InteractionType = EInteractionTypes::ROTATES;

	UPROPERTY(EditAnywhere)
		ELocks m_LockType = ELocks::UNLOCKED;

	bool Open = false;
	bool PlayerCanInteract = true;
	bool PlayerInRange = false;
	bool PlayerIsInteracting = false;
	bool IsOpening = false;
	bool IsClosing = false;

	

	UPROPERTY(EditAnywhere)
		FRotator FinalRotation;
	UPROPERTY(EditAnywhere)
		FVector OpeningValue;
	UPROPERTY(EditAnywhere)
		float TimeToOpen = 1;
	float PercentageRate = 0;
	float CurrentPercentage;

	FRotator m_Rotation;
	FRotator InitialDoorRotation;
	FRotator m_InitialRotatingValue;
	FQuat m_FinalRotation;

	FVector m_Location;
	FVector m_InitialLocation;
	FVector m_FinalLocation;

	void CheckRotationState();
	void CheckTranslationState();
	void CloseRotation();
	void CloseTranslation();
	void TryOpeningActor();
	void TryClosingActor();

	//Pressure plates if dependent
	UPROPERTY(EditAnywhere)
		TArray<APressurePlate_base*> m_dependentPressurePlates;
	float m_accumulatedMass;
	float m_totalMassNeeded;
	void CheckPressurePlatesStatus();
	float GetTotalCurrentMass();
	bool AllDependentPressurePlatesAreActive();

public:
	bool ActorHasFullyOpenedOrClosed();
	bool IsOpen();
	bool Rotates();
	void SetPlayerInRange(bool tempRange);

	
	inline ELocks GetDynamicType(){ return m_LockType; }
	void FlipFlopOpen();
};
